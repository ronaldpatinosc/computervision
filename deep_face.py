from deepface import DeepFace
from matplotlib import pyplot as plt

backends = ['opencv', 'ssd', 'dlib', 'mtcnn', 'retinaface']
models = ["VGG-Face", "Facenet", "Facenet512",
          "OpenFace", "DeepFace", "DeepID", "ArcFace", "Dlib"]


def detectFace(imagen=''):
    detected_face = None
    try:
        detected_face = DeepFace.detectFace(img_path=imagen,
                                            detector_backend=backends[4])
        #plt.imshow(detected_face, interpolation='nearest')
        # plt.show()
    except:
        print("No hay deteccion")
    finally:
        return detected_face


def findFace(face):
    found_face = None
    try:
        found_face = DeepFace.find(img_path=face,
                                   enforce_detection=False,
                                   model_name=models[0],
                                   db_path="db",
                                   detector_backend=backends[4])
        if found_face.shape[0] > 0:
            found_face = found_face.iloc[0].identity
        else:
            found_face = None
    except:
        print("No cara encontrada en la DB")
    finally:
        return found_face
