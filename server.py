import cv2
from google.protobuf.message import EncodeError
import imagezmq
import face
import objec_det as od
import deep_face as df
import time
import img_util


def main():
    image_hub = imagezmq.ImageHub()
    while True:
        cam_id, frame = image_hub.recv_image()
        frame = cv2.flip(frame, 1)
        #frame = face.Detection(frame)
        #frame = face.Mesh(frame)
        #frame = faceMesh(frame)
        frame = od.objectos(frame)
        # if frame is not None:
        face_detected = df.detectFace(frame)
        if face_detected is not None:
            print("tenemos cara")
            encuentra = df.findFace(face_detected)
            if encuentra is not None:
                print("tenemos cara FINDFACE " + encuentra)
                fileStr = "dump/" + time.ctime() + ".jpg"
                image = cv2.imread(encuentra)
                im_v = img_util.vconcat([frame, image])
                cv2.imwrite(fileStr, im_v)
                #cv2.imshow(cam_id, im_v)
                # cv2.waitKey(1)

        image_hub.send_reply(b'OK')


if __name__ == '__main__':
    main()
