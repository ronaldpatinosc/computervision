import mediapipe as mp
import cv2


def Mesh(frame):
    drawingModule = mp.solutions.drawing_utils
    faceModule = mp.solutions.face_mesh
    circleDrawingSpec = drawingModule.DrawingSpec(
        thickness=1, circle_radius=1, color=(0, 0, 255))
    lineDrawingSpec = drawingModule.DrawingSpec(thickness=1, color=(0, 255, 0))

    with faceModule.FaceMesh(max_num_faces=2, refine_landmarks=True) as face:
        frame.flags.writeable = False
        results = face.process(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        frame.flags.writeable = True
        if results.multi_face_landmarks != None:
            for faceLandmarks in results.multi_face_landmarks:
                drawingModule.draw_landmarks(
                    frame, faceLandmarks, faceModule.FACEMESH_CONTOURS, circleDrawingSpec, lineDrawingSpec)
    return frame


def Detection(frame):
    mp_face_detection = mp.solutions.face_detection
    mp_drawing = mp.solutions.drawing_utils
    with mp_face_detection.FaceDetection(model_selection=1, min_detection_confidence=0.6) as face_detection:
        frame.flags.writeable = False
        results = face_detection.process(frame)
        frame.flags.writeable = True
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        if results.detections:
            for detection in results.detections:
                mp_drawing.draw_detection(frame, detection)
    return frame
