import imagezmq
from imutils.video import VideoStream
import time
import socket

sender = imagezmq.ImageSender(connect_to='tcp://172.17.0.53:5555')
camera_name = socket.gethostname()  # send RPi hostname with each image

"""
https://towardsdatascience.com/live-video-streaming-using-multiple-smartphones-with-imagezmq-e260bd081224
path = "rtsp://192.168.1.70:8080//h264_ulaw.sdp"
cap = VideoStream(path)
Puede ser el path de acceso a una camara
"""
cap = VideoStream().start()
time.sleep(2.0)  # allow camera sensor to warm up
while True:  # send images as stream until Ctrl-C
    frame = cap.read()
    sender.send_image(camera_name, frame)
