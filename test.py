from re import match
from deepface import DeepFace
from matplotlib import pyplot as plt

#result = DeepFace.verify(img1_path="bill/img1.jpg", img2_path="bill/img2.jpg")
# result = DeepFace.analyze(img_path="bill/img4.jpg", actions=[
#                          'age', 'gender', 'race', 'emotion'])
# print(result)
# face verification
# obj = DeepFace.verify(img1_path="bill.jpg",
# img2_path="bill/img2.jpg", detector_backend=backends[4])

# face recognition
# df = DeepFace.find(img_path="bill.jpg", db_path="bill",
#                   detector_backend=backends[4])

# facial analysis
# demography = DeepFace.analyze(
#    img_path="bill/img4.jpg", detector_backend=backends[4])


backends = ['opencv', 'ssd', 'dlib', 'mtcnn', 'retinaface']
models = ["VGG-Face", "Facenet", "Facenet512",
          "OpenFace", "DeepFace", "DeepID", "ArcFace", "Dlib"]


# face detection and alignment
detected_face = DeepFace.detectFace(img_path="source/rpg.jpg",
                                    detector_backend=backends[4])

plt.imshow(detected_face, interpolation='nearest')
plt.show()

df = DeepFace.find(img_path=detected_face,
                   enforce_detection=False,
                   db_path="db",
                   detector_backend=backends[4],
                   model_name=models[0])

index = 0
if df.shape[0] > 0:
    match = df.iloc[0].identity
    print(match)


# face verification
obj = DeepFace.verify(img1_path="source/rpg.jpg",
                      img2_path="db/rpg/img3.jpg",
                      detector_backend=backends[4],
                      model_name="Facenet",
                      enforce_detection=False)

print(obj)
print(df)
plt.imshow(detected_face, interpolation='nearest')
plt.show()


# obj = DeepFace.analyze(img_path=detected_face, actions=[
#                       'age', 'gender', 'race', 'emotion'])
# print(obj)
